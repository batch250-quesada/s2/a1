package com.zuitt.example;

import java.util.Scanner;

public class LeapYear {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter year: ");
        int year = scanner.nextInt();
        boolean isLeapYear = (( year % 4 == 0 ) && ( year % 100 !=0 ) || ( year % 400 == 0 ));
        if (isLeapYear) System.out.println(year + " is a Leap year");
        else System.out.println(year + " is not Leap year");

    }
}
