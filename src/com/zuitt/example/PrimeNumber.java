package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class PrimeNumber {
    public static void main(String[] args) {
        // ArrayList
        ArrayList<String> myArrayList = new ArrayList<String>(Arrays.asList("Song, Hye, Kyo"));

        // HashMap
        HashMap<String, Integer> myHashMap = new HashMap<String, Integer>() {
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 5);
            }
        };

        // Array
        int [] primeArr = {2, 3, 5, 7, 11};

        System.out.println("@ARRAYLIST: My friends are: " + myArrayList);
        System.out.println("@HASHMAP: Inventory: " + myHashMap);

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter index of prime number: ");
        int p = scanner.nextInt();

        switch (p) {
            case 0:
                System.out.println("First prime number: " + primeArr[p]);
                break;
            case 1:
                System.out.println("Second prime number: " + primeArr[p]);
                break;
            case 2:
                System.out.println("Third prime number: " + primeArr[p]);
                break;
            case 3:
                System.out.println("Fourth prime number: " + primeArr[p]);
                break;
            case 4:
                System.out.println("Fifth prime number: " + primeArr[p]);
                break;
            default:
                System.out.println("Exception: out of bounds");
        }
    }
}
